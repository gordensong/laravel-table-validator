<?php


namespace GordenSong\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;
use GordenSong\MySQL\RuleGetter;

abstract class BaseRuleGetter implements RuleGetter
{
	protected function getSchemaType(Column $column)
	{
		$schema = $column->getCustomSchemaOption('schema');

		return $schema['Type'];
	}
}