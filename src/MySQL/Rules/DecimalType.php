<?php


namespace GordenSong\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class DecimalType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$rules[] = 'numeric';

		if ($column->getPrecision()) {
			if ($column->getPrecision() == 10 && $column->getScale() == 0) {

			} else {
				$expect = str_repeat('9', $column->getPrecision() - $column->getScale());
				if ($column->getScale()) {
					$expect .= '.' . str_repeat('9', $column->getScale());
				}
				$rules['max'] = 'max:' . $expect;
				$rules['min'] = 'min:-' . $expect;
			}
		}
		if ($column->getUnsigned()) {
			$rules['min'] = 'min:0';
		}

		return array_values($rules);
	}
}
