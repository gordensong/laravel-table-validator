<?php


namespace GordenSong\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class JsonType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		return ['json'];
	}
}