<?php


namespace GordenSong\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class BigIntType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$rules[] = 'integer';

		if ($column->getUnsigned()) {
			$rules[] = 'min:0';
		}

		return $rules;
	}
}