<?php

namespace GordenSong\MySQL;

use Doctrine\DBAL\Schema\Column;

interface RuleGetter
{
	public function rules(Column $column): array;
}