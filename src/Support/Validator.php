<?php

namespace GordenSong\Support;

use GordenSong\Utils\RuleUtil;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Traits\Macroable;

abstract class Validator implements Arrayable
{
	use ValidatorTrait, SceneTrait, PrefixTrait, InstanceTrait, Macroable;

	public function __construct()
	{
		$this->rules = RuleUtil::mergeManyRules($this->rules, $this->customizeRules());
		$this->rules = RuleUtil::diffManyRules($this->rules, $this->excludeRules());

		$this->normalizeScenes();
	}

	public function toArray(): array
	{
		return [
			'rules' => $this->rules(),
			'messages' => $this->messages(),
			'attributes' => $this->attributes(),
		];
	}
}
