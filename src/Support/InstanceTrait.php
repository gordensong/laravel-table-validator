<?php

namespace GordenSong\Support;

trait InstanceTrait
{
	public static function instance()
	{
		return new static();
	}
}