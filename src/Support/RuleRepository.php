<?php


namespace GordenSong\Support;


use GordenSong\Utils\TableMeta;
use Illuminate\Support\Facades\Log;
use Webmozart\Assert\Assert;

class RuleRepository
{
	/**
	 * @throws \GordenSong\Exceptions\TableNotExistException
	 * @throws \Doctrine\DBAL\Schema\SchemaException
	 */
	public static function getRules(string $connection, string $table): array
	{
		Assert::notNull($table);
		Assert::notNull($connection);

		$rules = RuleCache::getRules($connection, $table);
		if ($rules) {
			Log::channel('stack')->info("rule from cache $connection, $table");
			return $rules;
		}

		Log::channel('stack')->info("rule from db $connection, $table");
		return TableMeta::make($connection, $table)->getRules();
	}
}