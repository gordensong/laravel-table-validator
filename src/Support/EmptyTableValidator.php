<?php

namespace GordenSong\Support;

class EmptyTableValidator extends TableValidator
{
	public function __construct(string $table, string $connection = null)
	{
		$this->setTable($table, $connection ?? config('database.default'));

		parent::__construct();
	}

	private function setTable(string $table, string $connection = null)
	{
		$this->table = $table;
		$this->connection = $connection;
	}
}