<?php

namespace GordenSong\Providers;

use GordenSong\Console\Command\GenerateTableValidatorCacheCommand;
use GordenSong\Console\Command\GenerateTableValidatorCommand;
use GordenSong\Utils\PathUtil;
use Illuminate\Support\ServiceProvider;

class LaravelTableValidatorServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('command.table-validator-cache.generate', function ($app) {
			return new GenerateTableValidatorCacheCommand($app['files']);
		});

		$this->app->bind('command.table-validator.generate', function ($app) {
			return new GenerateTableValidatorCommand($app['files']);
		});

		$this->commands('command.table-validator-cache.generate');
		$this->commands('command.table-validator.generate');

		$this->mergeConfigFrom($this->configPath(), 'table-validator.php');
	}

	public function boot()
	{
		$configPath = $this->configPath();
		if (function_exists('config_path')) {
			$publishPath = config_path('table-validator.php');
		} else {
			$publishPath = base_path('config/table-validator.php');
		}
		$this->publishes([$configPath => $publishPath], 'config');
	}

	protected function configPath(): string
	{
		$configPath = __DIR__ . '/../../config/table-validator.php';
		return PathUtil::crossPlatformPath($configPath);
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides(): array
	{
		return [
			'command.table-validator-cache.generate',
			'command.table-validator.generate'
		];
	}

}
