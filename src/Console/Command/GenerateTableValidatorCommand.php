<?php

namespace GordenSong\Console\Command;

use GordenSong\Exceptions\BladeFileNotFoundException;
use GordenSong\Exceptions\FilePutContentException;
use GordenSong\Exceptions\TargetFileExistsException;
use GordenSong\Templates\TableValidatorTemplate;
use GordenSong\Utils\DatabaseUtil;
use Illuminate\Database\QueryException;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class GenerateTableValidatorCommand extends \Illuminate\Console\Command
{
	/**
	 * @var Filesystem $files
	 */
	protected $files;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'validator:make {table*} {--connection=mysql} {--force}';

	/**
	 * @var string
	 */
	protected $dir = 'app';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate one or more table validators';

	/**
	 * @var string
	 */
	protected $existingFactories = '';

	/**
	 * @var array
	 */
	protected $properties = [];

	/**
	 * @var
	 */
	protected $force;
	/**
	 * @var string|null
	 */
	private $connection;

	/**
	 * @param Filesystem $files
	 */
	public function __construct(Filesystem $files)
	{
		parent::__construct();
		$this->files = $files;
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->dir = $this->option('dir');
		$this->force = $this->option('force');
		$this->connection = $this->option('connection') ?: config('database.default');

		$tables = $this->argument('table');
		if ($tables === ['.']) {
			$tables = DatabaseUtil::allTableNames($this->connection, false);
		}

		if (count($tables) == 0) {
			$this->error('没有指定任何表');
			return;
		}

		foreach ($tables as $table) {
			if ($table === 'migrations') {
				continue;
			}
			try {
				$tableExist = DatabaseUtil::tableExist($this->connection, $table);
				if (!$tableExist) {
					$this->error("Table({$table}) don't exist");
					continue;
				}
			} catch (QueryException $e) {
				$this->error($e->getMessage());
				return;
			}

			$tableValidatorTemplate = new TableValidatorTemplate($table, $this->connection);

			$filename = $tableValidatorTemplate->targetFile();
			if ($this->files->exists($filename) && !$this->force) {
				$this->warn('Table validator exists, use --force to overwrite: ' . $filename);
				continue;
			}

			try {
				$tableValidatorTemplate->output($this->force);
				$this->info('Table validator created: ' . $filename);
			} catch (TargetFileExistsException | FilePutContentException | BladeFileNotFoundException $e) {
				$this->error($e->getMessage());
			}
		}
	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments(): array
	{
		return [
			['table', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Which models to include, "." means all tables', []],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions(): array
	{
		return [
			['dir', 'D', InputOption::VALUE_OPTIONAL, 'The validator directory', $this->dir],
			['force', 'F', InputOption::VALUE_NONE, 'Overwrite any existing validator'],
			['connection', 'C', InputOption::VALUE_OPTIONAL, 'set a connection name'],
		];
	}
}
