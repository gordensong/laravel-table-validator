<?php

namespace GordenSong\Utils;

use Webmozart\Assert\Assert;

class SceneUtil
{
	/**
	 * demo:
	 * <pre>
	 * $values = ['title', 'price' => 'required', 'bar_code' => ['size:13', 'required']]
	 * </pre>
	 * normalized:
	 * <pre>
	 * $values = [
	 *    'title' => [],
	 *    'price' => ['required'],
	 *    'bar_code' => ['size:13', 'required'],
	 * ]
	 * </pre>
	 * @param array $values
	 * @return array
	 */
	public static function normalize(array $values): array
	{
		$result = [];
		foreach ($values as $key => $value) {
			if (is_numeric($key)) {
				Assert::string($value);
				$result[$value] = [];
			} elseif (is_string($key)) {
				$result[$key] = (array)$value;
			}
		}
		return $result;
	}
}