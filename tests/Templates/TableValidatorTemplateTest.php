<?php


namespace Tests\Templates;


use GordenSong\Templates\TableValidatorTemplate;
use Tests\TestCase;

class TableValidatorTemplateTest extends TestCase
{

	/**
	 * @var TableValidatorTemplate
	 */
	private $template;

	protected function setUp(): void
	{
		parent::setUp(); // TODO: Change the autogenerated stub

		$this->template = new TableValidatorTemplate('user');
	}

	public function test_compileData()
	{
		$data = $this->template->getCompiledData();
		dump($data);

		self::assertArrayHasKey('namespace', $data);
		self::assertArrayHasKey('connection', $data);
		self::assertArrayHasKey('classname', $data);
		self::assertArrayHasKey('tablename', $data);
		self::assertArrayHasKey('rules', $data);
		self::assertArrayHasKey('carbonAt', $data);
	}

	public function test_compile()
	{
		$content = $this->template->compile();
		dump($content);
		self::assertStringContainsString('class UserValidator extends \GordenSong\Support\TableValidator', $content);
		self::assertStringContainsString('protected $connection = \'mysql\';', $content);
		self::assertStringContainsString('protected $table = \'user\';', $content);
		self::assertStringContainsString('public function customizeRules(): array', $content);
		self::assertStringContainsString('public function excludeRules(): array', $content);
		self::assertStringContainsString('protected $messages = [', $content);
		self::assertStringContainsString('protected $attributes = [', $content);
	}
}
