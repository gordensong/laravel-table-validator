<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\BooleanType;
use GordenSong\MySQL\Rules\TextType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TextTypeTest extends AbstractTypeRuleTest
{
	protected $class = TextType::class;

	public function test_text()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->text($this->field);
		});

		self::assertEquals(['string', 'max:65535'], $this->getRules());
	}

	public function test_mediumText()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->mediumText($this->field);
		});

		self::assertEquals(['string', 'max:16777215'], $this->getRules());
	}

	public function test_longText()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->longText($this->field);
		});

		self::assertEquals(['string'], $this->getRules());
	}
}
