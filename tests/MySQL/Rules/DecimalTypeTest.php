<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\DecimalType;
use Illuminate\Database\QueryException;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DecimalTypeTest extends AbstractTypeRuleTest
{
	protected $class = DecimalType::class;

	public function test_float()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->float($this->field, 4, 2);
		});

		self::assertEquals(['numeric', 'max:99.99', 'min:-99.99'], $this->getRules());

		try {
			DB::table($this->table)->insert([$this->field => '100']);
		} catch (QueryException $e) {
			self::assertStringContainsString('Numeric value out of range', $e->getMessage());
		}
	}

	public function test_unsignedFloat()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->unsignedFloat($this->field, 4, 2);
		});

		self::assertEquals(['numeric', 'max:99.99', 'min:0'], $this->getRules());

		try {
			DB::table($this->table)->insert([$this->field => '-1']);
		} catch (QueryException $e) {
			self::assertStringContainsString('Numeric value out of range', $e->getMessage());
		}
	}

	public function test_float_default_length()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->float($this->field);
		});

		self::assertEquals(['numeric', 'max:999999.99', 'min:-999999.99'], $this->getRules());
	}

	public function test_float_2_0()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->float($this->field, 2, 0);
		});

		DB::table($this->table)->insert([$this->field => 100]);

		self::assertEquals(['numeric'], $this->getRules());
	}

	public function test_double()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->double($this->field);
		});

		self::assertEquals(['numeric'], $this->getRules());
	}

	public function test_double_total()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->double($this->field, 1);
		});
		$this->getRules();
		self::assertEquals(['numeric'], $this->getRules());

		DB::table($this->table)->insert([$this->field => 1000000]);
		$value = DB::table($this->table)->value($this->field);

		self::assertGreaterThan(9999, $value);

		DB::table($this->table)->insert([$this->field => 9999999999]);
		$row = DB::table($this->table)->latest('id')->first();
		self::assertEquals(9999999999, $row->{$this->field});
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => $a = 99999999999.999]);
		$var = DB::table($this->table)->value($this->field);
		// dump($var);
		self::assertEquals($a, $var);

		DB::table($this->table)->insert([$this->field => $a = 99999999999999999999999999999999999]);
		$var = DB::table($this->table)->value($this->field);
		// dump($var);
	}

	public function test_double_total_place()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->double($this->field, 4, 2);
		});

		self::assertEquals(['numeric', 'max:99.99', 'min:-99.99'], $this->getRules());
	}

	public function test_decimal()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->decimal($this->field);
		});

		self::assertEquals(['numeric', 'max:999999.99', 'min:-999999.99'], $this->getRules());

		DB::table($this->table)->insert([$this->field => 999999.99]);
	}

	public function test_decimal_2_0()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->decimal($this->field, 2, 0);
		});

		self::assertEquals(['numeric', 'max:99', 'min:-99'], $this->getRules());

		DB::table($this->table)->insert([$this->field => 99]);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(99, $value);
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => 99.1]);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(99, $value);
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => 99.111111]);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(99, $value);
		DB::table($this->table)->truncate();

		try {
			DB::table($this->table)->insert([$this->field => 100]);
		} catch (QueryException $e) {
			self::assertStringContainsString('Numeric value out of range', $e->getMessage());
		}
	}

	/**
	 * 四舍五入后超过范围抛异常
	 */
	public function test_decimal_2_1()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->decimal($this->field, 2, 1);
		});

		self::assertEquals(['numeric', 'max:9.9', 'min:-9.9'], $this->getRules());

		DB::table($this->table)->insert([$this->field => 9.9]);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(9.9, $value);
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => -9.9]);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(-9.9, $value);
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => 0.99999]);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(1, $value);
		DB::table($this->table)->truncate();

		try {
			DB::table($this->table)->insert([$this->field => 9.99]);
		} catch (QueryException $e) {
			self::assertStringContainsString('Numeric value out of range', $e->getMessage());
		}
	}
}
