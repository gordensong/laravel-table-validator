<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\DateTimeType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class DateTimeTypeTest extends AbstractTypeRuleTest
{
	protected $class = DateTimeType::class;

	public function test_datetime()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->dateTime($this->field);
		});

		self::assertEquals(['date'], $this->getRules());

		DB::table($this->table)->insert([$this->field => '2016-01-01 10:00:00']);
		$value = DB::table($this->table)->value($this->field);
		self::assertSame('2016-01-01 10:00:00', $value);

		$validator = Validator::make($data = [$this->field => '2016-01-01 10:00:00'], [$this->field => 'date']);
		self::assertSame($data, $validator->validate());
	}
}
