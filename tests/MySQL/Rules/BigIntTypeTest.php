<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\BigIntType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BigIntTypeTest extends AbstractTypeRuleTest
{
	protected $class = BigIntType::class;

	public function test_autoincrement()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->bigIncrements($this->field);
		});

		self::assertEquals(['integer', 'min:0'], $this->getRules());
	}

	public function test_bigInteger()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->bigInteger($this->field);
		});

		self::assertEquals(['integer'], $this->getRules());
	}

	public function test_unsignedBigInteger()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger($this->field);
		});

		self::assertEquals(['integer', 'min:0',], $this->getRules());
	}
}
