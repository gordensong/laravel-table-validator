<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\BooleanType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BooleanTypeTest extends AbstractTypeRuleTest
{
	protected $class = BooleanType::class;

	public function test_autoincrement()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->tinyIncrements($this->field = 'id');
		});

		self::assertEquals(['integer', 'min:0', 'max:255'], $this->getRules());
	}

	public function test_boolean()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->boolean($this->field)->nullable(true);
		});

		self::assertEquals(['boolean'], $this->getRules());
	}

	public function test_tinyint()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->tinyInteger($this->field)->nullable(true);
		});

		self::assertEquals(['integer', 'min:-127', 'max:128'], $this->getRules());
	}

	public function test_unsigned_tinyint()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->unsignedTinyInteger($this->field = 'unsigned_tiny_integer_field');
		});

		self::assertSame(['integer', 'min:0', 'max:255'], $this->getRules());
	}
}
