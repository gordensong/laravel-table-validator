<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\FloatType;

class FloatRuleTest extends AbstractTypeRuleTest
{
	protected $class = FloatType::class;

	public function setUp(): void
	{
		parent::setUp();

		self::markTestSkipped();
	}
}
