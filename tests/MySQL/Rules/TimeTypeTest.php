<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\TimeType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class TimeTypeTest extends AbstractTypeRuleTest
{
	protected $class = TimeType::class;

	public function test_time()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->time($this->field);
		});

		self::assertSame([TimeType::REGEX], $this->getRules());

		DB::table($this->table)->insert($data = [$this->field => '12:34:56']);
		$value = DB::table($this->table)->value($this->field);
		self::assertSame('12:34:56', $value);

		$validated = Validator::make($data, [$this->field => TimeType::REGEX])->validated();
		self::assertSame($data, $validated);
	}

}
