<?php


namespace Tests\MySQL\Rules;


use GordenSong\MySQL\Rules\StringType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StringTypeTest extends AbstractTypeRuleTest
{
	protected $class = StringType::class;

	public function test_char()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->char($this->field, 30);
		});

		self::assertSame(['string', 'size:30'], $this->getRules());
	}

	public function test_string()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->string($this->field);
		});

		self::assertSame(['string', 'max:255'], $this->getRules());
	}

	public function test_string_length()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->string($this->field, 30);
		});

		self::assertSame(['string', 'max:30'], $this->getRules());
	}

	public function test_enum()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->enum($this->field, [1, 2]);
		});

		self::assertSame(['string'], $this->getRules());
	}

}
