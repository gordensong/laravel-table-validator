<?php

namespace Tests\MySQL;


use GordenSong\MySQL\RuleGetterFactory;
use GordenSong\Utils\TableMeta;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class RuleGetterFactoryTest extends TestCase
{
	use RefreshDatabase;

	public function test_create()
	{
		Schema::create('rule', function (Blueprint $table) {
			$table->id();
			$table->boolean('boolean_field');
		});

		$meta = TableMeta::make('mysql', 'rule');

		$column = $meta->getColumn('boolean_field');

		$ruleFactory = new RuleGetterFactory();
		$rules = $ruleFactory->make($column)->rules($column);

		self::assertIsArray($rules);

		Schema::dropIfExists('rule');
	}
}
