<?php

namespace Tests\Support\InvoiceDemo;

use GordenSong\Support\Validator;

class InvoiceAddressValidator extends Validator
{
	protected $rules = [
		'recipient_name' => [
			'required',
			'regex:/^[\w\x{4e00}-\x{9fa5}]{1,}$/iu'
		],
		'mobile' => [
			'required',
			'regex:/^1[3456789]\d{9}$/'
		],
		'province' => [
			'required',
			'regex:/^[\x{4e00}-\x{9fa5}]{1,}$/iu'
		],
		'city' => [
			'required',
			'regex:/^[\x{4e00}-\x{9fa5}]{1,}$/iu'
		],
		'district' => [
			'required',
			'regex:/^[\x{4e00}-\x{9fa5}]{1,}$/iu'
		],
		'address' => [
			'required',
		],
	];

	protected $messages = [
		'recipient_name.required' => '收件人不能为空',
		'province.required' => '省不能为空',
		'city.required' => '市不能为空',
		'district.required' => '区不能为空',
		'address.required' => '街道地址不能为空',
		'zip_code.required' => '邮编不能为空',
		'mobile.required' => '手机号不能为空',
		'mobile.regex' => '手机号格式错误',
		'qq.required' => 'qq不能为空',
		'qq.regex' => 'QQ号码不正确',
	];
}