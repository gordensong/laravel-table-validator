<?php

namespace Tests\Support\InvoiceDemo;

use GordenSong\Support\Validator;

class InvoiceInfoValidator extends Validator
{
	protected $rules = [
		'title' => [
			'required',
		],
		'tax_reg_number' => [
			'required',
			'regex:/^[A-Za-z0-9]{15}$|^[A-Za-z0-9]{17}$|^[A-Za-z0-9]{18}$|^[A-Za-z0-9]{20}$/'
		],
		'invoice_type' => [
			'required',
			'in:1,2,3'
		],
		'register_telephone' => [
			'required',
			'regex:/^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/i'
		],
		'bank_name' => [
			'required',
			'regex:/^[\w\x{4e00}-\x{9fa5}]{1,}$/iu'
		],
		'bank_account' => [
			'required',
			'regex:/^\d{1,}$/'
		],
	];

	protected $scenes = [
		'person' => ['title', 'tax_reg_number', 'invoice_type'],
		'company' => ['title', 'tax_reg_number', 'invoice_type', 'register_telephone', 'bank_name', 'bank_account'],
	];

	protected $messages = [
		'title.required' => '发票类型错误',
		'title.regex' => '抬头格式错误',
		'tax_reg_number.required' => '税务登记证号不能为空',
		'tax_reg_number.regex' => '税务登记证号格式错误',
		'bank_name.required' => '开户银行名称不能为空',
		'bank_name.regex' => '开户银行格式不正确',
		'bank_account.required' => '开户账号不能为空',
		'bank_account.regex' => '开户账号格式错误',
		'invoice_type.required' => '发票类型不能为空',
		'invoice_type.in' => '发票类型错误',
	];
}