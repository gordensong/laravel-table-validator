<?php

namespace Tests\Support\InvoiceDemo;

use GordenSong\Support\Collection;
use Tests\TestCase;

class InvoiceDemoTest extends TestCase
{
	public function test_scene()
	{
		$v = (new InvoiceInfoValidator())->scene('person');
		dump($v->rules());
		self::assertCount(3, $rules = $v->rules());
		self::assertArrayHasKey('title', $rules);
		self::assertArrayHasKey('tax_reg_number', $rules);
		self::assertArrayHasKey('invoice_type', $rules);
	}

	public function test_collection_scene()
	{
		$collection = Collection::make([
			(new OidsValidator()),
			(new InvoiceInfoValidator())->scene('person')->prefix('invoice_info'),
			(new InvoiceAddressValidator())->prefix('address')
		]);

		$rules = $collection->rules();
		dump($rules);
		self::assertArrayHasKey('oid', $rules);
		self::assertArrayHasKey('oid.*', $rules);
		self::assertArrayHasKey('invoice_info.title', $rules);
		self::assertArrayHasKey('address.address', $rules);

		dump($collection->rules(), $collection->messages(), $collection->attributes());
	}
}