<?php

namespace Tests\Support\InvoiceDemo;

use GordenSong\Support\Validator;

class OidsValidator extends Validator
{
	protected $rules = [
		'oid' => [
			'required',
			'array'
		],
		'oid.*' => [
			'required',
			'Integer'
		],
	];
}