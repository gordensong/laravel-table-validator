<?php

namespace Tests\Support\InvoiceDemo;

use GordenSong\Support\Validator;

class EmailValidator extends Validator
{
	protected $rules = [
		'email' => [
			'required',
			'regex:/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/i'
		]
	];

	protected $messages = [
		'email.required' => '邮箱不能为空',
		'email.regex' => '邮箱格式不正确',
	];
}