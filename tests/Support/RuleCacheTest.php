<?php

namespace Tests\Support;


use GordenSong\Support\RuleCache;
use GordenSong\Templates\TableValidatorCacheTemplate;
use GordenSong\Utils\PathUtil;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RuleCacheTest extends TestCase
{
	use RefreshDatabase;

	public function test_cache()
	{
		$template = new TableValidatorCacheTemplate($connection = 'mysql', $table = 'book');
		$cacheFile = $template->targetFile();

		$content = <<<PHP
<?php
return [
	'title' => ['string'],
];
PHP;
		/** @var Filesystem $file */
		$file = app(Filesystem::class);
		$file->ensureDirectoryExists(dirname($cacheFile));
		$file->put($cacheFile, $content);

		$data = RuleCache::cache($connection, $table);

		self::assertEquals([
			'title' => ['string'],
		], $data);

		$file->delete($cacheFile);
	}

	public function test_cacheFile()
	{
		$connection = 'mysql';
		$table = 'book';

		$suffix = "bootstrap/cache/table-validator/{$connection}/{$table}.php";
		$suffix = PathUtil::crossPlatformPath($suffix);

		self::assertStringEndsWith($suffix, RuleCache::cacheFile($connection, $table));
		echo RuleCache::cacheFile($connection, $table);
	}
}
