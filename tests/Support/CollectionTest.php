<?php

namespace Tests\Support;


use GordenSong\Support\Collection;
use GordenSong\Support\Validator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\Factory;
use Tests\TestCase;
use Tests\Validators\AddressValidator;
use Tests\Validators\AuthorValidator;
use Tests\Validators\Mysql\UserAddressValidator;
use Tests\Validators\Mysql\UserInfoValidator;
use Tests\Validators\Mysql\UserValidator;
use Tests\Validators\SchemaTableValidator;

class CollectionTest extends TestCase
{
	use RefreshDatabase;

	public function test_make()
	{
		$collection = Collection::make([new UserValidator(), new UserInfoValidator()]);

		self::assertTrue($collection->hasKey('username'));
		self::assertTrue($collection->hasKey('xing'));
	}

	public function test_add()
	{
		$collection = Collection::make()->add(new UserValidator);
		self::assertTrue($collection->hasKey('username'));

		$collection->add(new UserInfoValidator());
		self::assertTrue($collection->hasKey('xing'));
		self::assertTrue($collection->hasKey('ming'));
	}

	public function test_prefix()
	{
		$validateCollection = Collection::make()
			->add(new UserValidator)
			->prefix('data');

		self::assertArrayHasKey('data.username', $validateCollection->rules());
		self::assertArrayHasKey('data.password', $validateCollection->rules());
	}

	public function test_macro()
	{
		Validator::macro('validate', function (array $data) {
			/** @var Factory $factory */
			$factory = app(Factory::class);

			/** @var Validator $this */
			return $factory->validate($data, $this->rules(), $this->messages(), $this->attributes());
		});

		$data = [
			'username' => 'abc',
			'password' => '123',
		];
		$validated = (new UserValidator())->exclude(['id'])->validate($data);
		dump($validated);

		self::assertEquals($data, $validated);
	}

	public function test_nest()
	{
		$collection = Collection::make([
			UserValidator::instance()->prefix('author'),
			Collection::make([
				UserAddressValidator::instance()->prefix('address.*'),
			]),
		])->prefix('base');

		$rules = $collection->rules();

		self::assertArrayHasKey('base.author', $rules);
		self::assertArrayHasKey('base.address.*', $rules);

		dump($rules);
	}
}
