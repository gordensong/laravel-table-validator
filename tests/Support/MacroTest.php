<?php

namespace Tests\Support;

use GordenSong\Support\Collection;
use GordenSong\Support\Validator;
use Illuminate\Validation\Factory;
use Tests\TestCase;
use Tests\Validators\AuthorValidator;
use Tests\Validators\Mysql\UserValidator;

class MacroTest extends TestCase
{
	public function test_validator()
	{
		Validator::macro('validate', function (array $data) {
			/** @var Factory $factory */
			$factory = app(Factory::class);

			/** @var Validator $this */
			return $factory->validate($data, $this->rules(), $this->messages(), $this->attributes());
		});

		$data = [
			'username' => 'abc',
			'password' => '123',
		];
		$validator = UserValidator::instance()->exclude(['id']);
		dump($validator->rules());
		$validated = $validator->validate($data);

		self::assertSame($data, $validated);
	}

	public function test_Collection()
	{
		Collection::macro('validate', function (array $data) {
			/** @var Factory $factory */
			$factory = app(Factory::class);

			/** @var Validator $this */
			return $factory->validate($data, $this->rules(), $this->messages(), $this->attributes());
		});

		$data = [
			'username' => 'abc',
			'password' => '123',
		];
		$validated = Collection::make([UserValidator::instance()->exclude(['id'])])->validate($data);

		self::assertSame($data, $validated);
	}
}