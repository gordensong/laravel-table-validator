<?php

namespace Tests\Validators;

use GordenSong\Support\TableValidator;

class BookValidator extends TableValidator
{
	protected $rules = [
		'price' => ['min:0'],
		'title' => ['min:5'],
	];

	public function customizeRules(): array
	{
		return [
			'price' => ['min:0'],
			'title' => ['min:5'],
		];
	}

	protected $messages = [
		'price.min' => ':attribute 价格必须大于等于 0',
		'title.min' => ':attribute 长度必须大于等于 5',
	];

	protected $attributes = [
		'id' => 'ID',
		'title' => '书名',
		'price' => '价格',
	];
}
