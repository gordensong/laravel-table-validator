<?php

namespace Tests\Validators\Mysql;

class UserValidator extends \GordenSong\Support\TableValidator
{
    protected $connection = 'mysql';
    protected $table = 'user';

    public function customizeRules(): array
    {
        return [
            'id' => ['required'],
            'username' => ['required', 'max:30', 'min:3'],
            'password' => ['required'],
            'created_at' => [],
            'updated_at' => [],
            'deleted_at' => [],
        ];
    }

    public function excludeRules(): array
    {
        return [
            'created_at',
            'updated_at',
            'deleted_at',
	        'username' => ['min'],
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [

    ];

    protected $scenes = [

    ];
}
