<?php

namespace Tests\Validators\Validator;

use GordenSong\Support\Validator;

class QqValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'qq' => ['numeric']
		];
	}
}