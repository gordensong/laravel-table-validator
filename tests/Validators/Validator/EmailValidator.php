<?php

namespace Tests\Validators\Validator;

use GordenSong\Support\Validator;

class EmailValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'email' => ['string', 'email']
		];
	}
}