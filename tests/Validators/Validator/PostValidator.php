<?php

namespace Tests\Validators\Validator;

use GordenSong\Support\Validator;

class PostValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'id' => ['integer'],
			'user_id' => ['integer'],
		];
	}
}