<?php

namespace Tests\Validators\Validator;

use GordenSong\Support\Validator;

class AddressValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'province' => ['string'],
			'city' => ['string'],
			'district' => ['string'],
			'street' => ['string'],
		];
	}
}