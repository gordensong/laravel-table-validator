<?php

namespace Tests\Utils;


use GordenSong\Utils\RuleUtil;
use Tests\TestCase;

class RuleUtilTest extends TestCase
{
	public function test_normalizeRule()
	{
		$array = ['string', 'min:2', 'min:1', 'required'];

		$rules = RuleUtil::normalizeRule($array);
		dump($rules);

		self::assertEquals(['required', 'string', 'min:1'], $rules);
	}

	public function test_normalizeManyRules()
	{
		$rules = ['title' => ['string'], 'price' => ['string', 'required']];

		$rules = RuleUtil::normalizeManyRules($rules);
		dump($rules);

		self::assertEquals(['title' => ['string'], 'price' => ['required', 'string']], $rules);
	}

	public function test_getKeyRules_callback()
	{
		$callback = function () {
			echo 1;
		};
		$callback2 = function () {
			echo 2;
		};
		$array = [$callback, $callback2, 'min:2', 'min:1', 'required'];

		$rules = RuleUtil::getKeyRules($array);
		dump($rules);

		self::assertSame([$callback, $callback2, 'min' => 'min:1', 'required' => 'required'], $rules);
	}

	public function test_flatten()
	{
		$rules = ['a|b', ['d', 'e:f', ['g', 'h|i']]];

		$result = RuleUtil::flatten($rules);

		dump($result);

		self::assertSame(['a|b', 'd', 'e:f', 'g', 'h|i'], $result);
	}

	public function test_mergeRules()
	{
		$callback = function () {
		};
		$rule1 = ['string', 'max:1', 'min:2', 'min:1', 'required', $callback];
		$rule2 = ['max:1', 'min:2', 'min:3', 'required', $callback];

		$rules = RuleUtil::mergeRules($rule1, $rule2);

		dump($rules);

		self::assertSame(['string' => 'string', 'max' => 'max:1', 'min' => 'min:3', 'required' => 'required', $callback, $callback], $rules);
	}

	public function test_diffRules()
	{
		$rules = RuleUtil::diffRules(['string', 'required'], ['string']);
		self::assertSame(['required'], $rules);

		$rules = ['required', 'max:100'];
		$rules2 = ['max'];

		$diffRules = RuleUtil::diffRules($rules, $rules2);
		self::assertSame(['required'], $diffRules);
	}

	public function test_diffManyRules()
	{
		$rules = ['title' => ['string', 'required'], 'price' => ['numeric', 'max:100']];
		$rules2 = ['title' => ['string'], 'price' => ['max']];

		$diffRules = RuleUtil::diffManyRules($rules, $rules2);
		self::assertSame(['title' => ['required'], 'price' => ['numeric']], $diffRules);
	}

	public function test_diffManyRules_rm_field()
	{
		$rules = ['title' => ['string', 'required'], 'created_at' => ['datetime']];
		$rules2 = ['title' => ['string'], 'created_at'];

		$diffRules = RuleUtil::diffManyRules($rules, $rules2);
		self::assertSame(['title' => ['required']], $diffRules);
	}
}
