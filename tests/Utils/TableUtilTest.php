<?php

namespace Tests\Utils;

use Doctrine\DBAL\Schema\Table;
use GordenSong\Exceptions\TableNotExistException;
use GordenSong\Utils\TableUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class TableUtilTest extends TestCase
{
	use RefreshDatabase;

	private $connection = 'mysql';

	public function test_load()
	{
		$table = TableUtil::load($this->connection, 'user');
		dump($table);

		self::assertInstanceOf(Table::class, $table);
		self::assertEquals('ims_user', $table->getName());
	}
}
