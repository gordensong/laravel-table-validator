<?php

namespace Tests\Utils;


use GordenSong\Utils\PathUtil;
use Tests\TestCase;

class PathUtilTest extends TestCase
{
	public function test_crossPlatformPath()
	{
		$path = '/a/b/c.php';
		self::assertSame($path, PathUtil::crossPlatformPath($path));

		$path = <<<WINDOWS_PATH
c:\a\b\c.php
WINDOWS_PATH;
;
		self::assertSame('c:/a/b/c.php', PathUtil::crossPlatformPath($path));
	}
}
