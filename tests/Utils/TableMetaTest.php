<?php


namespace Tests\Utils;


use GordenSong\Utils\TableMeta;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TableMetaTest extends TestCase
{
	use RefreshDatabase;

	/**
	 * @var TableMeta
	 */
	private $meta;

	protected function setUp(): void
	{
		parent::setUp();

		$this->meta = new TableMeta('mysql', 'user');
	}

	public function test_toArray()
	{
		$rules = $this->meta->getRules();

		self::assertIsArray($rules);
		self::assertNotEmpty($rules);
		foreach ($rules as $rule) {
			self::assertGreaterThanOrEqual(1, count($rule));
		}

		// dump($rules);
	}

	public function test_getTableName()
	{
		self::assertEquals('user', $this->meta->getTableName());
	}

	public function test_getConnection()
	{
		self::assertEquals('mysql', $this->meta->getConnectionName());
		self::assertEquals('Mysql', $this->meta->getConnectionNamespace());
	}

	public function test_getCarbonAtFields()
	{
		self::assertEquals([
			'created_at',
			'updated_at',
			'deleted_at',
		], $this->meta->getCarbonAtFields());
	}
}
