<?php

namespace Tests\Utils;

use GordenSong\Utils\SceneUtil;
use Tests\TestCase;

class SceneUtilTest extends TestCase
{
	public function test_normalize()
	{
		$values = ['title', 'price' => 'required', 'bar_code' => ['size:13', 'required']];
		$normalized = SceneUtil::normalize($values);

		self::assertEquals([
			'title' => [],
			'price' => ['required'],
			'bar_code' => ['size:13','required'],
		], $normalized);
	}
}