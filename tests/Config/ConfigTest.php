<?php

namespace Tests\Config;

use GordenSong\Templates\TableValidatorCacheTemplate;
use GordenSong\Utils\PathUtil;
use Tests\TestCase;

class ConfigTest extends TestCase
{
	public function test_cache()
	{
		/** @var \Illuminate\Config\Repository $config */
		$config = app('config');
		$config->set('table-validator', [
			'cache' => [
				'path' => base_path('bootstrap/cache/table-validator'),
			],
		]);

		foreach ([
					 TableValidatorCacheTemplate::configCachePath(),
					 TableValidatorCacheTemplate::defaultCachePath(),
					 TableValidatorCacheTemplate::cachePath(),
				 ] as $item) {
			self::assertStringEndsWith(base_path(PathUtil::crossPlatformPath('bootstrap/cache/table-validator')), $item);
		}
	}

	public function test_()
	{
		dump(storage_path('cache'));

		self::assertStringEndsWith(PathUtil::crossPlatformPath('/storage/cache'), storage_path('cache'));
	}
}