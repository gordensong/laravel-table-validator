<?php

namespace Tests\ValidatorTest\Mysql;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Validators\Mysql\UserValidator;

class UserValidatorTest extends TestCase
{
	use RefreshDatabase;

	protected function tearDown(): void
	{
		parent::tearDown(); // TODO: Change the autogenerated stub

		$this->markTestSkipped();
	}

	public function test_rules()
	{
		$rules = UserValidator::instance()->rules();
		dump($rules);
	}

	public function test_prefix()
	{
		$validator = UserValidator::instance()->prefix('user');

		dump($validator->rules());
	}

	public function test_prefix_star()
	{
		$validator = UserValidator::instance()->prefix('users.*', true);

		dump($validator->rules());
	}

	public function test_addRule()
	{
		$validator = UserValidator::instance()->addRule('password_confirm', ['required']);

		dump($validator->rules());
	}

	public function test_exclude()
	{
		$validator = UserValidator::instance()->exclude(['id', 'password' => ['required']]);

		dump($validator->rules());
	}

	public function test_scene()
	{
		$validator = UserValidator::instance()->scene();

		dump($validator->rules());
	}
}