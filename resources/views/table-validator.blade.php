namespace App\Validators\{{$namespace}};

class {{$classname}}Validator extends \GordenSong\Support\TableValidator
{
    protected $connection = '{{$connection}}';
    protected $table = '{{$tablename}}';

    public function customizeRules(): array
    {
        return [
@foreach($columns as $column)
            '{{$column->getName()}}' => [@if ($column->getNotnull())'required'@endif],
@endforeach
        ];
    }

    public function excludeRules(): array
    {
        return [
@foreach($carbonAt as $field)
            '{{$field}}',
@endforeach
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [

    ];

    protected $scenes = [

    ];
}
