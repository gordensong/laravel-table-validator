return [
@foreach($rules as $field => $rule)
    '{{$field}}' => [
@foreach($rule as $item)
        '{!! $item !!}',
@endforeach
    ],
@endforeach
];
