<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfoTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_info', function (Blueprint $table) {
			$table->id();
			$table->integer('user_id')->nullable(false)->default(0);
			$table->string('xing')->nullable(true)->comment('姓');
			$table->string('ming')->nullable(true)->comment('名');
			$table->integer('age')->default(0);
			$table->json('config1');
			$table->text('config2')->comment('序列化字段');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_info');
	}
}
